<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>    
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" type="text/css" href="./css/styles.css">
  <title>Twitter  finder</title>
</head>
<body>

  <?php
  require_once('./class/tweet.php');
  $tweet = new tweet();


  ?>
  <main class="container-fluid no-gutters">
    <section class="row-fluid no-gutters title">
      <div class="col-md-6">
        
        <h1> <i class="fa fa-envelope"></i> Tweet finder </h1>
      </div>
      <div class="col-md-6">
        <a href="index.php" class="btn btn-info"><i class="fa fa-search"></i>Find tweets</a>
        <a href="trending.php" class="btn btn-success"><i class="fa fa-arrow-up"></i>View trending</a>
      </div>
    </section>

    <section class="row-fluid no-gutters table-content">
      <div class="col-md-12">
        <table class="form form-striped" id="tabl">
          <thead>
            <tr>
              <th>Date</th>
              <th>Time</th>
              <th>Tweet</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($tweet->getAll() as $i => $twitt):


            $time = date("H:i:s",strtotime($twitt['date']));
            $date = date("Y-m-d",strtotime($twitt['date']));


            ?>
            <tr>
              <td><?php echo $date;?></td>
              <td><?php echo $time;?></td>
              <td><?php echo $twitt['word'];?></td>

            </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </div>
  </section>



</main>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

<script>

  $(document).ready(function() {
    $('#tabl').DataTable();
  } );

</script>

</body>
</html>