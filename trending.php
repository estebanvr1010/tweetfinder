<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>    
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="./css/styles.css">

  <title>Twitter finder / Trending</title>
</head>
<body>

  <?php
  require_once('./class/tweet.php');
  $tweet = new tweet();

  $json = json_encode($tweet->getAll());


  ?>
  <main class="container-fluid no-gutters">
    <section class="row-fluid no-gutters title">
      <div class="col-md-6">

       <h1> <i class="fa fa-envelope"></i> Tweet finder </h1>
     </div>
     <div class="col-md-6">
      <a href="index.php" class="btn btn-info"><i class="fa fa-search"></i>Find tweets</a>
      <a href="trending.php" class="btn btn-success"><i class="fa fa-arrow-up"></i>View trending</a>
    </div>
  </section>

  <section class="row-fluid no-gutters char-content">
    <div class="col-md-12">
      <div id="chartdiv"></div>
    </div>
  </section>



</main>

<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){

    $.cols = '';

    $.ajax({

      url: 'http://localhost/tweetfinder/get/json_all_tweets_cols.php',
      method: 'GET',
      success:function(data) {
        $.cols  = data;

      }

    });

    $.ajax({
      url: 'http://localhost/tweetfinder/get/json_all_tweets.php',
      method: 'GET',
      success: function(data){

        const CHART = document.getElementById('lineChart');
        var cols  = JSON.parse($.cols);

        var lineChart = new Chart(CHART, {
          type: 'line',
          data: {
            labels: cols,
            datasets:[
            {
              label: 'My first dataset',
              fill: false,
              lineTension: 0,
              backgroundColor: "rgba(75,192,192,0.4)",
              borderColor: "rgba(75,192,192,1)",
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJointStyle: 'miter',

              data: [65,59,80,81,56,55]
            }
            ]
          }
        });




      }



    });


  });




</script>


<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.js"></script>

<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <canvas id="lineChart" width="400" height="400"></canvas>
    </div>
    <div class="col-sm-6">
    </div>
  </div>
</div>


<script>






//    am4core.ready(function() {





//}); // end am4core.ready()
</script>
</body>
</html>