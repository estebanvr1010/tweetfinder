<?php
require_once('../class/tweet.php');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
$tweets = new tweet();

echo json_encode($tweets->getAllJson());
exit();